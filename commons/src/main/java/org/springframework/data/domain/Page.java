package org.springframework.data.domain;

import java.util.List;
import java.util.function.Function;

public interface Page<T> {
    int getNumber();

    int getSize();

    int getNumberOfElements();

    List<T> getContent();

    int getTotalPages();

    long getTotalElements();

    <U> Page<U> map(Function<? super T, ? extends U> converter);
}
