package com.athenacenter.athena.commons.oauth;

import com.athenacenter.athena.commons.oauth.impl.OAuthClientBuilder;

import java.util.concurrent.CompletableFuture;

public interface OAuthClient {
    CompletableFuture<AccessToken> getAccessToken();

    static OAuthClientBuilder builder() {
        return new OAuthClientBuilder();
    }
}
