package com.athenacenter.athena.commons.oauth.impl;

import com.athenacenter.athena.commons.oauth.AccessToken;
import com.athenacenter.athena.commons.oauth.OAuthClient;
import lombok.RequiredArgsConstructor;

import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
public class CachedOAuthClient implements OAuthClient {
    private final OAuthClient oAuthClient;
    private volatile CompletableFuture<AccessToken> tokenFut;


    @Override
    public CompletableFuture<AccessToken> getAccessToken() {
        var fut = this.tokenFut;
        if (needRefreshToken(fut)) {
            fut = oAuthClient.getAccessToken();
            this.tokenFut = fut;
        }
        return fut;
    }

    public CompletableFuture<AccessToken> refreshAccessToken() {
        tokenFut = oAuthClient.getAccessToken();
        return tokenFut;
    }

    private boolean needRefreshToken(CompletableFuture<AccessToken> accessTokenFut) {
        return accessTokenFut == null
                || (accessTokenFut.isDone() && (accessTokenFut.isCompletedExceptionally() || accessTokenFut.join().isExpired()));
    }
}
