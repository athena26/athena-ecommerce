package com.athenacenter.athena.commons.exception;

public class OAuthException extends RuntimeException {
    public OAuthException(String message) {
        super(message);
    }
}
