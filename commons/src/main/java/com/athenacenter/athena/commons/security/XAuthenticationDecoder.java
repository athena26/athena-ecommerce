package com.athenacenter.athena.commons.security;

import com.athenacenter.athena.commons.model.AuthAccount;
import com.athenacenter.athena.commons.util.Json;
import com.dslplatform.json.JsonReader;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.List;

@Component
public class XAuthenticationDecoder {
    private static final JsonReader.ReadObject<AuthAccount> account = Json.findReader(AuthAccount.class);

    public Authentication decode(String token) {
        byte[] decoded = Base64.getUrlDecoder().decode(token);
        var principal = Json.decode(decoded, account);
        return new UsernamePasswordAuthenticationToken(principal, token, List.of());
    }
}
