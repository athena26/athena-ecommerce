create table account
(
    id            serial8      not null
        constraint account_pkey
            primary key,
    gg_id         varchar(50)  not null unique,
    email         varchar(100) not null unique,
    name          varchar(100) not null,
    id_token      varchar(1024),
    token         varchar(1024),
    refresh_token varchar(2048),
    created_at    timestamptz default CURRENT_TIMESTAMP,
    updated_at    timestamptz default CURRENT_TIMESTAMP
);

create index account_email_idx on account (email);
create type account_type as (id bigint, gg_id varchar(50), email varchar(100), name varchar(100), id_token varchar(1024), token varchar(1024), refresh_token varchar(2048), created_at timestamptz, updated_at timestamptz );

