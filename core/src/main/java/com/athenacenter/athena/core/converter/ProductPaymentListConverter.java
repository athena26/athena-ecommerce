package com.athenacenter.athena.core.converter;

import com.athenacenter.athena.commons.util.Json;
import com.athenacenter.athena.core.model.ProductPayment;
import com.dslplatform.json.JsonReader;
import com.dslplatform.json.JsonWriter;
import com.dslplatform.json.runtime.Generics;

import javax.persistence.AttributeConverter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ProductPaymentListConverter implements AttributeConverter<List<ProductPayment>, String> {
    private static final JsonReader.ReadObject<List<ProductPayment>> metadataReader = Json.findReader(Generics.makeParameterizedType(List.class, ProductPayment.class));
    private static final JsonWriter.WriteObject<List<ProductPayment>> metadataWriter = Json.findWriter(Generics.makeParameterizedType(List.class, ProductPayment.class));

    @Override
    public String convertToDatabaseColumn(List<ProductPayment> metadata) {
        return metadata != null ? Json.encodeToString(metadata, metadataWriter) : null;
    }

    @Override
    public List<ProductPayment> convertToEntityAttribute(String dbData) {
        return dbData != null ? Json.decode(dbData.getBytes(StandardCharsets.UTF_8), metadataReader) : null;
    }
}
