package com.athenacenter.athena.core.model.entity;

import com.athenacenter.athena.core.model.ShopStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Data
@Accessors(chain = true)
public class Shop extends BaseEntity<Shop> {
    @Id
    @SequenceGenerator(name = "shop_id_seq", sequenceName = "shop_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "shop_id_seq")
    Long id;
    Long owner;
    String name;
    String description;
    String credential;
    @Enumerated(value = EnumType.STRING)
    ShopStatus status;
    LocalTime open_time;
    LocalTime close_time;
    boolean suspended;

    @Override
    protected Shop self() {
        return this;
    }
}
