package com.athenacenter.athena.core.controller;

import com.athenacenter.athena.core.model.request.LoginRequest;
import com.athenacenter.athena.core.model.response.LoginResponse;
import com.athenacenter.athena.core.service.AuthService;
import com.athenacenter.athena.commons.model.response.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/auth/")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/login")
    public Response<LoginResponse> login(@RequestBody @Valid LoginRequest loginRequest) throws IOException {
        return Response.ofSucceeded(authService.login(loginRequest));
    }
}
