package com.athenacenter.athena.core.converter;

import com.athenacenter.athena.commons.util.Json;
import com.athenacenter.athena.core.model.DeliveryProgress;
import com.dslplatform.json.JsonReader;
import com.dslplatform.json.JsonWriter;
import com.dslplatform.json.runtime.Generics;

import javax.persistence.AttributeConverter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class DeliveryProgressConverter implements AttributeConverter<List<DeliveryProgress>, String> {
    private static final JsonReader.ReadObject<List<DeliveryProgress>> metadataReader = Json.findReader(Generics.makeParameterizedType(List.class, DeliveryProgress.class));
    private static final JsonWriter.WriteObject<List<DeliveryProgress>> metadataWriter = Json.findWriter(Generics.makeParameterizedType(List.class, DeliveryProgress.class));

    @Override
    public String convertToDatabaseColumn(List<DeliveryProgress> metadata) {
        return metadata != null ? Json.encodeToString(metadata, metadataWriter) : null;
    }

    @Override
    public List<DeliveryProgress> convertToEntityAttribute(String dbData) {
        return dbData != null ? Json.decode(dbData.getBytes(StandardCharsets.UTF_8), metadataReader) : null;
    }
}
