package com.athenacenter.athena.core.model;

import com.dslplatform.json.CompiledJson;
import com.github.longdt.dsljson.SnakeCaseNaming;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@CompiledJson(namingStrategy = SnakeCaseNaming.class)
public class PropertyContent {
    private String content;
    private boolean displayed;
    private Long price;
    private Long cost;
    private Long totalAmount;
    private Long purchasedAmount;
}
