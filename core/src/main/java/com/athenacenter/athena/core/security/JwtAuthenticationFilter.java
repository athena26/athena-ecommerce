package com.athenacenter.athena.core.security;

import com.hazelcast.core.HazelcastInstance;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Log4j2
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private static final String BEARER_TOKEN_PREFIX = "Bearer ";
    private static final int TOKEN_START_INDEX = BEARER_TOKEN_PREFIX.length();
    private final JwtService jwtService;
    private final Long defaultValidAccountCacheTtl;

    public JwtAuthenticationFilter(HazelcastInstance hazelcastInstance, JwtService jwtService,
                                   @Value("${castle.account.cache.ttl:#{1800}}") Long defaultValidAccountCacheTtl) {
        this.jwtService = jwtService;
        this.defaultValidAccountCacheTtl = defaultValidAccountCacheTtl;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        final var header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null || !header.startsWith(BEARER_TOKEN_PREFIX)) {
            filterChain.doFilter(request, response);
            return;
        }
        final var token = header.substring(TOKEN_START_INDEX);
        var authentication = jwtService.decode(token);
        if (authentication != null) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        filterChain.doFilter(request, response);
    }

}
