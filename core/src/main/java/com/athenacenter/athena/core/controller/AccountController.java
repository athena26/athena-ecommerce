package com.athenacenter.athena.core.controller;

import com.athenacenter.athena.core.service.AccountService;
import com.athenacenter.athena.commons.model.response.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts/")
@RequiredArgsConstructor
public class AccountController {
    private final AccountService accountService;

    @GetMapping("/login/link")
    public Response<String> getLinkLogin() {
        return Response.ofSucceeded(accountService.getLinkLogin());
    }


}
