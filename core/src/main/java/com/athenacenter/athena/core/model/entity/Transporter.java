package com.athenacenter.athena.core.model.entity;

import com.athenacenter.athena.core.converter.PostageConverter;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Map;

@Entity
@Data
@Accessors(chain = true)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Transporter extends BaseEntity<Transporter> {
    @Id
    @SequenceGenerator(name = "transporter_id_seq", sequenceName = "transporter_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transporter_id_seq")
    Long id;
    String name;
    @Convert(converter = PostageConverter.class)
    Map<String, Long> postage;


    @Override
    protected Transporter self() {
        return this;
    }
}
