package com.athenacenter.athena.core.converter;

import com.athenacenter.athena.commons.util.Json;
import com.dslplatform.json.JsonReader;
import com.dslplatform.json.JsonWriter;
import com.dslplatform.json.runtime.Generics;

import javax.persistence.AttributeConverter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class PostageConverter implements AttributeConverter<Map<String, Long>, String> {
    private static final JsonReader.ReadObject<Map<String, Long>> metadataReader = Json.findReader(Generics.makeParameterizedType(Map.class, String.class, Long.class));
    private static final JsonWriter.WriteObject<Map<String, Long>> metadataWriter = Json.findWriter(Generics.makeParameterizedType(Map.class, String.class, Long.class));

    @Override
    public String convertToDatabaseColumn(Map<String, Long> metadata) {
        return metadata != null ? Json.encodeToString(metadata, metadataWriter) : null;
    }

    @Override
    public Map<String, Long> convertToEntityAttribute(String dbData) {
        return dbData != null ? Json.decode(dbData.getBytes(StandardCharsets.UTF_8), metadataReader) : null;
    }
}
