package com.athenacenter.athena.core.repository;

import com.athenacenter.athena.core.model.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query(value = """
        with new_account as (
            insert into account (email, gg_id, name, id_token, token, refresh_token, created_at, updated_at)
                        values
                            (:#{#account.email}, :#{#account.ggId}, :#{#account.name}, :#{#account.idToken}, :#{#account.token},
                             :#{#account.refreshToken}, :#{#account.createdAt}, :#{#account.updatedAt})
                        on conflict (email) do nothing returning * 
        )
        select (coalesce(
                    (select row(id, gg_id, email, name, id_token, token, refresh_token, created_at, updated_at)\\:\\:account_type from new_account),
                    (select row(id, gg_id, email, name, id_token, token, refresh_token, created_at, updated_at)\\:\\:account_type from account where email = :#{#account.email} )
                    )).*
        """, nativeQuery = true)
    @Transactional
    Account getOrCreate(Account account);
}
