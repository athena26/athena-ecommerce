package com.athenacenter.athena.core.service.impl;

import com.athenacenter.athena.core.repository.AccountRepository;
import com.athenacenter.athena.core.service.AccountServiceInternal;
import com.athenacenter.athena.core.model.entity.Account;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountServiceImpl implements AccountServiceInternal {
    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public String getLinkLogin() {
        return null;
    }

    @Override
    @Transactional
    public Account getOrCreate(Account account) {
        return accountRepository.getOrCreate(account);
    }
}
