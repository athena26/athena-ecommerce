package com.athenacenter.athena.core.service;

import com.athenacenter.athena.core.model.entity.Account;

public interface AccountServiceInternal extends AccountService {
    Account getOrCreate(Account account);
}
