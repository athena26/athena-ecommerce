package com.athenacenter.athena.core.model;


public enum ProductStatus {
    PENDING, LISTING, STOP_SELLING
}
