package com.athenacenter.athena.core.model.entity;

import com.athenacenter.athena.core.model.AccountStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@Accessors(chain = true)
public class Account extends BaseEntity<Account> {
    @Id
    @SequenceGenerator(name = "account_id_seq", sequenceName = "account_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_id_seq")
    Long id;
    Long profileId;
    Long firebaseId;
    String phone;
    String email;
    String password;
    @Enumerated(value = EnumType.STRING)
    AccountStatus status;

    @Override
    protected Account self() {
        return this;
    }
}
