package com.athenacenter.athena.core.converter;

import com.athenacenter.athena.commons.util.Json;
import com.athenacenter.athena.core.model.ProductProperties;
import com.dslplatform.json.JsonReader;
import com.dslplatform.json.JsonWriter;
import com.dslplatform.json.runtime.Generics;

import javax.persistence.AttributeConverter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ProductPropertiesConverter implements AttributeConverter<List<ProductProperties>, String> {
    private static final JsonReader.ReadObject<List<ProductProperties>> metadataReader = Json.findReader(Generics.makeParameterizedType(List.class, ProductProperties.class));
    private static final JsonWriter.WriteObject<List<ProductProperties>> metadataWriter = Json.findWriter(Generics.makeParameterizedType(List.class, ProductProperties.class));

    @Override
    public String convertToDatabaseColumn(List<ProductProperties> metadata) {
        return metadata != null ? Json.encodeToString(metadata, metadataWriter) : null;
    }

    @Override
    public List<ProductProperties> convertToEntityAttribute(String dbData) {
        return dbData != null ? Json.decode(dbData.getBytes(StandardCharsets.UTF_8), metadataReader) : null;
    }
}
