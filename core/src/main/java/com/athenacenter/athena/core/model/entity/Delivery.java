package com.athenacenter.athena.core.model.entity;

import com.dslplatform.json.CompiledJson;
import com.github.longdt.dsljson.SnakeCaseNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@Accessors(chain = true)
@CompiledJson(namingStrategy = SnakeCaseNaming.class)
public class Delivery extends BaseEntity<Delivery> {
    @Id
    @SequenceGenerator(name = "delivery_id_seq", sequenceName = "delivery_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "delivery_id_seq")
    Long id;
    String name;

    @Override
    protected Delivery self() {
        return this;
    }
}
