package com.athenacenter.athena.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi
@SpringBootApplication
@ComponentScan({"com.athenacenter.athena.core", "com.athenacenter.athena.commons"})
public class VhtDecryptApplication {

    public static void main(String[] args) {
        SpringApplication.run(VhtDecryptApplication.class, args);
    }

}
