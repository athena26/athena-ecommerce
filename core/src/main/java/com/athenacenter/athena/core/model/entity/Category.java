package com.athenacenter.athena.core.model.entity;

import com.dslplatform.json.CompiledJson;
import com.github.longdt.dsljson.SnakeCaseNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@Accessors(chain = true)
@CompiledJson(namingStrategy = SnakeCaseNaming.class)
public class Category extends BaseEntity<Category> {
    @Id
    @SequenceGenerator(name = "category_id_seq", sequenceName = "category_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_id_seq")
    Long id;
    String name;

    @Override
    protected Category self() {
        return this;
    }
}
