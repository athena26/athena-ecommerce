package com.athenacenter.athena.core.model;

import com.dslplatform.json.CompiledJson;
import com.github.longdt.dsljson.SnakeCaseNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@CompiledJson(namingStrategy = SnakeCaseNaming.class)
public class ProductProperties {
    private String label;
    private List<PropertyContent> content;
    private String iconUrl;
}
