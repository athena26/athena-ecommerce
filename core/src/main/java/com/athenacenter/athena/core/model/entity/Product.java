package com.athenacenter.athena.core.model.entity;

import com.athenacenter.athena.core.converter.CategoryListConverter;
import com.athenacenter.athena.core.converter.ListStringConverter;
import com.athenacenter.athena.core.converter.ProductPropertiesConverter;
import com.athenacenter.athena.core.model.ProductProperties;
import com.athenacenter.athena.core.model.ProductStatus;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Accessors(chain = true)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Product extends BaseEntity<Product> {
    @Id
    @SequenceGenerator(name = "product_id_seq", sequenceName = "product_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_id_seq")
    Long id;
    Long shopId;
    String name;
    String titlePrice;
    @Convert(converter = ListStringConverter.class)
    List<String> imageUrls;
    String description;
    @Enumerated(value = EnumType.STRING)
    ProductStatus status;
    @Convert(converter = ProductPropertiesConverter.class)
    @Type(type = "jsonb")
    List<ProductProperties> properties;
    @Convert(converter = CategoryListConverter.class)
    @Type(type = "jsonb")
    List<Category> categories;

    @Override
    protected Product self() {
        return this;
    }
}
