package com.athenacenter.athena.core.model;

public enum AccountStatus {
    PENDING, ACTIVE, SUSPENDED, DELETED
}
