package com.athenacenter.athena.core.converter;

import com.athenacenter.athena.commons.util.Json;
import com.dslplatform.json.JsonReader;
import com.dslplatform.json.JsonWriter;
import com.dslplatform.json.runtime.Generics;

import javax.persistence.AttributeConverter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ListStringConverter implements AttributeConverter<List<String>, String> {
    private static final JsonReader.ReadObject<List<String>> metadataReader = Json.findReader(Generics.makeParameterizedType(List.class, String.class));
    private static final JsonWriter.WriteObject<List<String>> metadataWriter = Json.findWriter(Generics.makeParameterizedType(List.class, String.class));

    @Override
    public String convertToDatabaseColumn(List<String> metadata) {
        return metadata != null ? Json.encodeToString(metadata, metadataWriter) : null;
    }

    @Override
    public List<String> convertToEntityAttribute(String dbData) {
        return dbData != null ? Json.decode(dbData.getBytes(StandardCharsets.UTF_8), metadataReader) : null;
    }
}
