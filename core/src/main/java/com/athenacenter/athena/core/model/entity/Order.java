package com.athenacenter.athena.core.model.entity;


import com.athenacenter.athena.core.converter.DeliveryProgressConverter;
import com.athenacenter.athena.core.converter.ProductPaymentListConverter;
import com.athenacenter.athena.core.converter.TransporterConverter;
import com.athenacenter.athena.core.model.DeliveryProgress;
import com.athenacenter.athena.core.model.OrderStatus;
import com.athenacenter.athena.core.model.ProductPayment;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Accessors(chain = true)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Order extends BaseEntity<Order> {
    @Id
    @SequenceGenerator(name = "order_id_seq", sequenceName = "order_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_id_seq")
    Long id;
    Long accountId;
    @Convert(converter = ProductPaymentListConverter.class)
    @Type(type = "jsonb")
    List<ProductPayment> products;
    @Convert(converter = TransporterConverter.class)
    Transporter transporter;
    String beginLocation;
    String destinationLocation;
    @Enumerated(value = EnumType.STRING)
    OrderStatus status;
    @Convert(converter = DeliveryProgressConverter.class)
    List<DeliveryProgress> deliveryProgresses;

    @Override
    protected Order self() {
        return this;
    }
}
