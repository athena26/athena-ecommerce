package com.athenacenter.athena.core.model.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Accessors(chain = true)
public class Profile extends BaseEntity<Profile> {
    @Id
    @SequenceGenerator(name = "profile_id_seq", sequenceName = "profile_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profile_id_seq")
    Long id;
    String name;
    String address;
    LocalDate birthday;

    @Override
    protected Profile self() {
        return this;
    }
}
