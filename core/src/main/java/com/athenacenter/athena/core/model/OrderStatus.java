package com.athenacenter.athena.core.model;

public enum OrderStatus {
    PENDING, PAYING, WAITING_SHIPPER, SHIPPING, COMPLETE
}
