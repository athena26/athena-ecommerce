package com.athenacenter.athena.core.model.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
public class LoginRequest {
    @NotBlank
    private String code;
}
