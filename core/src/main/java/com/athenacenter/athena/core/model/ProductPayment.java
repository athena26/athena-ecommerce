package com.athenacenter.athena.core.model;

import com.dslplatform.json.CompiledJson;
import com.github.longdt.dsljson.SnakeCaseNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@CompiledJson(namingStrategy = SnakeCaseNaming.class)
public class ProductPayment {
    private Long id;
    private String name;
    private String description;
    private Long price;
    private List<ProductProperties> productProperties;
}
