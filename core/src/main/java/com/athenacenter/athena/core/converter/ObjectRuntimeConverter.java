package com.athenacenter.athena.core.converter;

import com.dslplatform.json.JsonConverter;
import com.dslplatform.json.JsonReader;
import com.dslplatform.json.JsonWriter;
import com.dslplatform.json.ObjectConverter;

@JsonConverter(target = Object.class)
public class ObjectRuntimeConverter {
    public static final JsonReader.ReadObject<Object> JSON_READER = r -> {
        if (r.wasNull()) return null;
        return ObjectConverter.deserializeObject(r);
    };
    public static final JsonWriter.WriteObject<Object> JSON_WRITER = (writer, value) -> {
        if (value != null) writer.serializeObject(value);
        else writer.writeNull();
    };
}
