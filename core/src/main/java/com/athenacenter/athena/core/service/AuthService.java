package com.athenacenter.athena.core.service;

import com.athenacenter.athena.core.model.request.LoginRequest;
import com.athenacenter.athena.core.model.response.LoginResponse;

import java.io.IOException;

public interface AuthService {
    LoginResponse login(LoginRequest loginRequest) throws IOException;
}
