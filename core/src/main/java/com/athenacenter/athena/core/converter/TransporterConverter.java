package com.athenacenter.athena.core.converter;

import com.athenacenter.athena.commons.util.Json;
import com.athenacenter.athena.core.model.entity.Transporter;
import com.dslplatform.json.JsonReader;
import com.dslplatform.json.JsonWriter;

import javax.persistence.AttributeConverter;
import java.nio.charset.StandardCharsets;

public class TransporterConverter implements AttributeConverter<Transporter, String> {
    private static final JsonReader.ReadObject<Transporter> metadataReader = Json.findReader(Transporter.class);
    private static final JsonWriter.WriteObject<Transporter> metadataWriter = Json.findWriter(Transporter.class);

    @Override
    public String convertToDatabaseColumn(Transporter metadata) {
        return metadata != null ? Json.encodeToString(metadata, metadataWriter) : null;
    }

    @Override
    public Transporter convertToEntityAttribute(String dbData) {
        return dbData != null ? Json.decode(dbData.getBytes(StandardCharsets.UTF_8), metadataReader) : null;
    }
}
