package com.athenacenter.athena.core.model;

public enum ShopStatus {
    ACTIVE, CLOSED
}
