package com.athenacenter.athena.core.converter;

import com.athenacenter.athena.commons.util.Json;
import com.athenacenter.athena.core.model.entity.Category;
import com.dslplatform.json.JsonReader;
import com.dslplatform.json.JsonWriter;
import com.dslplatform.json.runtime.Generics;

import javax.persistence.AttributeConverter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class CategoryListConverter implements AttributeConverter<List<Category>, String> {
    private static final JsonReader.ReadObject<List<Category>> metadataReader = Json.findReader(Generics.makeParameterizedType(List.class, Category.class));
    private static final JsonWriter.WriteObject<List<Category>> metadataWriter = Json.findWriter(Generics.makeParameterizedType(List.class, Category.class));

    @Override
    public String convertToDatabaseColumn(List<Category> metadata) {
        return metadata != null ? Json.encodeToString(metadata, metadataWriter) : null;
    }

    @Override
    public List<Category> convertToEntityAttribute(String dbData) {
        return dbData != null ? Json.decode(dbData.getBytes(StandardCharsets.UTF_8), metadataReader) : null;
    }
}
