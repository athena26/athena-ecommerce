package com.athenacenter.athena.core.model.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@Accessors(chain = true)
public class Address extends BaseEntity<Address> {
    @Id
    @SequenceGenerator(name = "address_id_seq", sequenceName = "address_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_id_seq")
    Long id;
    Long accountId;
    String alias;
    String address;
    Double lat;
    Double lon;

    @Override
    protected Address self() {
        return this;
    }
}
