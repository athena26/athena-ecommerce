package com.athenacenter.athena.core.service.impl;

import com.athenacenter.athena.core.model.entity.Account;
import com.athenacenter.athena.core.model.request.LoginRequest;
import com.athenacenter.athena.core.model.response.LoginResponse;
import com.athenacenter.athena.core.security.JwtService;
import com.athenacenter.athena.core.service.AccountServiceInternal;
import com.athenacenter.athena.core.service.AuthService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService {
    private static final long JWT_TTL_SECONDS = 30 * 24 * 60 * 60L;
    public final JwtService jwtService;
    public final AccountServiceInternal accountService;

    public AuthServiceImpl(AccountServiceInternal accountService, JwtService jwtService) throws IOException {
        this.accountService = accountService;
        this.jwtService = jwtService;
    }

    @Override
    @Transactional
    public LoginResponse login(LoginRequest loginRequest) throws IOException {
//        var ggToken = googleClient.getTokenResponse(loginRequest.getCode());
//        var loggedInUser = googleClient.getUser(ggToken.getAccessToken()).join();
//        var now = OffsetDateTime.now();
//        var account = accountService.getOrCreate(new Account().setEmail(loggedInUser.getEmail())
//                .setGgId(loggedInUser.getId())
//                .setName(loggedInUser.getName())
//                .setIdToken(ggToken.getIdToken())
//                .setToken(ggToken.getAccessToken())
//                .setRefreshToken(ggToken.getRefreshToken())
//                .setCreatedAt(now)
//                .setUpdatedAt(now));
//        return generateLoginResponse(account);
        return null;
    }

    private LoginResponse generateLoginResponse(Account account) {
        var iatSeconds = System.currentTimeMillis() / 1000;
        var expiredAt = iatSeconds + JWT_TTL_SECONDS;
        Map<String, Object> claims = new HashMap<>();
        claims.put("id", account.getId());
        claims.put("google_id", account.getEmail());
        claims.put("email", account.getEmail());
        claims.put("iat", iatSeconds);
        claims.put("exp", expiredAt);
        var token = jwtService.generateToken(claims);
        return new LoginResponse().setToken(token)
                .setExpiredAt(expiredAt);
    }
}
