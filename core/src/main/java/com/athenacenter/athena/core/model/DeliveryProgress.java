package com.athenacenter.athena.core.model;

import com.dslplatform.json.CompiledJson;
import com.github.longdt.dsljson.SnakeCaseNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

@Data
@Accessors(chain = true)
@CompiledJson(namingStrategy = SnakeCaseNaming.class)
public class DeliveryProgress {
    private String title;
    private OffsetDateTime completedAt;
    private String description;

}
