package com.athenacenter.athena.core.controller;

import com.athenacenter.athena.core.test.ApplicationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AgentControllerTest extends ApplicationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void decrypt() throws Exception {
        var request = MockMvcRequestBuilders.get("/agents/decryption/d0de3f9687b5f31682d6d6e248125b1b");
        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", notNullValue()))
                .andExpect(jsonPath("$.data", equalTo("0387120238")));
    }

}