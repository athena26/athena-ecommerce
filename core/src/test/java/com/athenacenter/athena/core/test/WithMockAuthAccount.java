package com.athenacenter.athena.core.test;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockAuthAccountSecurityContextFactory.class)
public @interface WithMockAuthAccount {
    long id() default 1L;

    String email() default "abc@gmail.com";

    String address() default "";
}
