package com.athenacenter.athena.core.test;

import com.athenacenter.athena.commons.model.AuthAccount;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.List;

public class WithMockAuthAccountSecurityContextFactory implements WithSecurityContextFactory<WithMockAuthAccount> {
    @Override
    public SecurityContext createSecurityContext(WithMockAuthAccount annotation) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        var principal = new AuthAccount().setId(annotation.id())
                .setEmail(annotation.email());
        if (!annotation.address().isEmpty()) {
            principal.setAddress(annotation.address());
        }
        var auth = new UsernamePasswordAuthenticationToken(principal, "password", List.of());
        context.setAuthentication(auth);
        return context;
    }
}
